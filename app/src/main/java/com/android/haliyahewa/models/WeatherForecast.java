package com.android.haliyahewa.models;

/**
 * Created by papalee on 9/11/17.
 */

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherForecast implements Parcelable
{

    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Double message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<com.android.haliyahewa.models.List> list = null;
    @SerializedName("city")
    @Expose
    private City city;
    public final static Parcelable.Creator<WeatherForecast> CREATOR = new Creator<WeatherForecast>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WeatherForecast createFromParcel(Parcel in) {
            WeatherForecast instance = new WeatherForecast();
            instance.cod = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.cnt = ((Integer) in.readValue((Integer.class.getClassLoader())));
            in.readList(instance.list, (com.android.haliyahewa.models.List.class.getClassLoader()));
            instance.city = ((City) in.readValue((City.class.getClassLoader())));
            return instance;
        }

        public WeatherForecast[] newArray(int size) {
            return (new WeatherForecast[size]);
        }

    };

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<com.android.haliyahewa.models.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.android.haliyahewa.models.List> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cod);
        dest.writeValue(message);
        dest.writeValue(cnt);
        dest.writeList(list);
        dest.writeValue(city);
    }

    public int describeContents() {
        return 0;
    }

}
