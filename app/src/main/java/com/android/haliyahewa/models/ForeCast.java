package com.android.haliyahewa.models;

/**
 * Created by papalee on 9/20/17.
 */


// for each future day entry
// get the short name of that day e.g Thur
// get the day of the month e.g 21
// get the number of the month e.g 09
// get the max temperature e.g 21.5
// get the mininum temperature e.g 18.3

public class ForeCast {
    private String weekDayName;
    private int dayOfMonth;
    private int month;
    private Double maxTemp;
    private Double minTemp;

    public ForeCast(int _dayOfMonth, int _month, String _weekDayName, Double _minTemp, Double _maxTemp){
        dayOfMonth = _dayOfMonth;
        month = _month;
        weekDayName = _weekDayName;
        minTemp = _minTemp;
        maxTemp = _maxTemp;
    }

    public String getWeekDayName() {
        return weekDayName;
    }

    public void setWeekDayName(String weekDayName) {
        this.weekDayName = weekDayName;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }
}
