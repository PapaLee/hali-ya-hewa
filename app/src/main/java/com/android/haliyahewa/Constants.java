package com.android.haliyahewa;

/**
 * Created by papalee on 9/12/17.
 */

public class Constants {
    public static final String OWM_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String OWM_BASE_ICON_URL = "http://openweathermap.org/img/w/";
    public static final String OWM_API_KEY = "c6e381d8c7ff98f0fee43775817cf6ad";
    public static final String DEFAULT_CITY = "Nairobi";
    public static final String DEFAULT_LAT = "-1.28333";
    public static final String DEFAULT_LON = "36.81667";
    public static final String DEFAULT_UNITS = "metric";
    public static final String WEATHER_ARGS = "WEATHER_ARGS";
    public static final String WEATHER_PREFS = "weather_preferences";
    public static final String BOOKMARK_KEY = "LocationBookMarkList";
    public static final String BOOKMARKLIST_KEY = "LocationBookMarkList";
    public static final String LISTFRAGMENT_TAG = "LocationListFragment";
    public static final String DETAILFRAGMENT_TAG = "LocationDetailFragment";
    public static final String MAPFRAGMENT_TAG = "LocationMapFragment";
    public static final String HELPFRAGMENT_TAG = "HelpFragment";
    public static final String SETTINGSFRAGMENT_TAG = "SettingsFragment";
}
