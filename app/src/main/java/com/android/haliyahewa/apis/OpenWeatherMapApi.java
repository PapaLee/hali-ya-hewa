package com.android.haliyahewa.apis;

import com.android.haliyahewa.models.WeatherContainer;
import com.android.haliyahewa.models.WeatherForecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by papalee on 9/12/17.
 */

public interface OpenWeatherMapApi {
    //http://api.openweathermap.org/data/2.5/forecast?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric
    @GET("forecast?")
    Call<WeatherForecast> getFiveDayForecast (@Query("lat") String latitude,
                                              @Query("lon") String longitude,
                                              @Query("appid") String appid,
                                              @Query("units") String units);

    //http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric
    @GET("weather?")
    Call<WeatherContainer> getDayForecast (@Query("lat") String latitude,
                                           @Query("lon") String longitude,
                                           @Query("appid") String appid,
                                           @Query("units") String units);
}
