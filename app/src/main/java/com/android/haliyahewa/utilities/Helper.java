package com.android.haliyahewa.utilities;

/**
 * Created by papalee on 9/20/17.
 */

public class Helper {

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

}
