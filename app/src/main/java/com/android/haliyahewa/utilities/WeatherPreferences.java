package com.android.haliyahewa.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.haliyahewa.Constants;
import com.android.haliyahewa.models.Coord;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.android.haliyahewa.Constants.BOOKMARKLIST_KEY;

/**
 * Created by papalee on 9/16/17.
 */

public class WeatherPreferences {
    private SharedPreferences mSharedPref;
    private Gson mGson;
    private final String TAG = getClass().getSimpleName();

    public WeatherPreferences(Context context) {
        mSharedPref = context.getSharedPreferences(Constants.WEATHER_PREFS, Context.MODE_PRIVATE);
        mGson = new Gson();
    }

    public void addLocationBookMark(Coord coord){
        // is there an existing list of bookmarked locations

        ArrayList<Coord> coordArrayList;

        if (mSharedPref.contains(BOOKMARKLIST_KEY)){
            // if yes add coord to that list

            coordArrayList = getLocationBookMarkList(BOOKMARKLIST_KEY);

            if(coordArrayList != null){
                if(!coordArrayList.contains(coord)){
                    coordArrayList.add(coord);
                    setLocationBookMarkList(Constants.BOOKMARKLIST_KEY, coordArrayList);
                }
            }

        } else{
            // if not, then create new list and add coord to it

            coordArrayList = new ArrayList<Coord>();
            coordArrayList.add(coord);
            setLocationBookMarkList(Constants.BOOKMARKLIST_KEY, coordArrayList);
        }
    }

    public void deleteLocationFromBookMark(final Coord coord){
        ArrayList<Coord> coordArrayList;

        if (mSharedPref.contains(BOOKMARKLIST_KEY)){

            coordArrayList = getLocationBookMarkList(BOOKMARKLIST_KEY);

            // delete the bookmarked location from shared preferences
            if(coordArrayList != null){
                if(coordArrayList.contains(coord)){
                    coordArrayList.remove(coord);
                    setLocationBookMarkList(Constants.BOOKMARKLIST_KEY, coordArrayList);
                } else{
                    Log.d(TAG, "Could not get Cord object");
                }
            }

        }
    }

    public void setLocationBookMarkList(String key, ArrayList<Coord> coordArrayList) {
        String coordListJson = mGson.toJson(coordArrayList);
        mSharedPref.edit().putString(key, coordListJson).apply();
    }

    public ArrayList<Coord> getLocationBookMarkList(String key){
        String coordListString = mSharedPref.getString(key, "");
        Type type = new TypeToken<ArrayList<Coord>>(){}.getType();
        return mGson.fromJson(coordListString, type);
    }

    public boolean isKeyPresent(String key){
        if(key != null){
            return mSharedPref.contains(key);
        } else{
            return false;
        }
    }
}
