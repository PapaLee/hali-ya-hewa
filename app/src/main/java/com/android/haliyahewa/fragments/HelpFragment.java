package com.android.haliyahewa.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.android.haliyahewa.R;

/**
 * Created by papalee on 9/14/17.
 */

public class HelpFragment extends DialogFragment {
    WebView mWebView;

    public HelpFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_help, container, false);
        mWebView = (WebView) v.findViewById(R.id.webview_help);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //load the local html file in the assets folder
        try {
            mWebView.loadUrl("file:///android_asset/help.html");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
