package com.android.haliyahewa.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.haliyahewa.R;

/**
 * Created by papalee on 9/18/17.
 */

public class SettingsFragment extends PreferenceFragment {
    public SettingsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        return v;
    }
}
