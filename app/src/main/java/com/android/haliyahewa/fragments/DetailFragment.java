package com.android.haliyahewa.fragments;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.haliyahewa.Constants;
import com.android.haliyahewa.R;
import com.android.haliyahewa.adapters.ForeCastAdapter;
import com.android.haliyahewa.apis.OpenWeatherMapApi;
import com.android.haliyahewa.models.Coord;
import com.android.haliyahewa.models.ForeCast;
import com.android.haliyahewa.models.List;
import com.android.haliyahewa.models.WeatherContainer;
import com.android.haliyahewa.models.WeatherForecast;
import com.android.haliyahewa.utilities.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.android.haliyahewa.Constants.DEFAULT_UNITS;
import static com.android.haliyahewa.Constants.OWM_API_KEY;

/**
 * Created by papalee on 9/18/17.
 */

public class DetailFragment extends Fragment {
    private WeatherContainer mWeatherContainer;
    private ForeCastAdapter mAdapter;
    private TextView tvLocationName;
    private TextView tvDate;
    private TextView tvTemperature;
    private TextView tvWeatherDescription;
    private TextView tvWind;
    private TextView tvHumidity;
    private TextView tvRain;
    private View mView;
    private RecyclerView mRecyclerView;
    private TextView mTvError;
    private ProgressBar mProgressBar;
    private static final String TAG = DetailFragment.class.getSimpleName();

    public DetailFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_location_detail, container, false);
        tvTemperature = (TextView) mView.findViewById(R.id.tv_temperature);
        tvWeatherDescription = (TextView) mView.findViewById(R.id.tv_weather_description);
        tvWind = (TextView) mView.findViewById(R.id.tv_wind);
        tvHumidity = (TextView) mView.findViewById(R.id.tv_humidity);
        mTvError = (TextView) mView.findViewById(R.id.tv_error);
        mProgressBar = (ProgressBar) mView.findViewById(R.id.pb_busy);
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_forecast);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mWeatherContainer = savedInstanceState.getParcelable(Constants.WEATHER_ARGS);
            initalizeUI(mWeatherContainer);
            return;
        }

        Bundle args = getArguments();
        if (args != null) {

            //get the weather container object passed through the bundle and populate the views
            mWeatherContainer = args.getParcelable(Constants.WEATHER_ARGS);
            initalizeUI(mWeatherContainer);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the weather container
        outState.putParcelable(Constants.WEATHER_ARGS, mWeatherContainer);
    }

    private void showErrorView(){
        //hide the list, show the error
        mRecyclerView.setVisibility(View.INVISIBLE);
        mTvError.setVisibility(View.VISIBLE);
    }

    private void initalizeUI(WeatherContainer weatherContainer){
        // populate the widgets with weather data and fetch the 5 day forecast asynchronously

        if(weatherContainer != null){

            try {
                String locationName;

                if(!mWeatherContainer.getName().isEmpty() && !mWeatherContainer.getSys().getCountry().isEmpty())
                {
                    locationName = mWeatherContainer.getName() + ", " + mWeatherContainer.getSys().getCountry();
                } else {
                    locationName = mWeatherContainer.getSys().getCountry();
                }

                //set the title of the detail page
                CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) mView.findViewById(R.id.toolbar_layout);
                if (appBarLayout != null) {
                    appBarLayout.setTitle(locationName);
                }

                long temp = Math.round(Math.floor(mWeatherContainer.getMain().getTemp()));
                tvTemperature.setText(temp + "°");
                tvWeatherDescription.setText(Helper.capitalizeFirstLetter(weatherContainer.getWeather().get(0).getDescription()));
                tvWind.setText(mWeatherContainer.getWind().getSpeed().toString());
                tvHumidity.setText(mWeatherContainer.getMain().getHumidity().toString());

                mAdapter = new ForeCastAdapter(new ArrayList<ForeCast>());
                mRecyclerView.setAdapter(mAdapter);

                // fetch the five day forecast
                fetchForeCast(weatherContainer.getCoord());

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void fetchForeCast(Coord coord){

        try {

            Log.d(TAG, "fetchForeCast starting");

            mProgressBar.setVisibility(View.VISIBLE);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.OWM_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            OpenWeatherMapApi apiService = retrofit.create(OpenWeatherMapApi.class);
            Call<WeatherForecast> weatherForecastCall = apiService.getFiveDayForecast(coord.getLat().toString(), coord.getLon().toString(), OWM_API_KEY, DEFAULT_UNITS);

            weatherForecastCall.enqueue(new retrofit2.Callback<WeatherForecast>() {
                @Override
                public void onResponse(Call<WeatherForecast> call, retrofit2.Response<WeatherForecast> response) {
                    if(response.isSuccessful()) {

                        //get the five day forecast
                        WeatherForecast fiveDayWeatherForecast = response.body();

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SSSS", Locale.getDefault());
                        //Date todayDate = Calendar.getInstance().getTime();

                        //get the date specified in the weatherContainer Object and use it as the start date
                        String dateMilliseconds = mWeatherContainer.getDt() + "000";
                        Calendar startDateCal = Calendar.getInstance();
                        startDateCal.setTimeInMillis(Long.parseLong(dateMilliseconds));
                        Date todayDate = startDateCal.getInstance().getTime();

                        try {

                            //initialize the arrays that store forecasts for each day as well as min/max temperatures for each day
                            ArrayList<List> dayOneList = new ArrayList<List>();
                            ArrayList<List> dayTwoList = new ArrayList<List>();
                            ArrayList<List> dayThreeList = new ArrayList<List>();
                            ArrayList<List> dayFourList = new ArrayList<List>();
                            ArrayList<List> dayFiveList = new ArrayList<List>();
                            ArrayList<Double> dayOneMaxTempList = new ArrayList<Double>();
                            ArrayList<Double> dayOneMinTempList = new ArrayList<Double>();
                            ArrayList<Double> dayTwoMaxTempList = new ArrayList<Double>();
                            ArrayList<Double> dayTwoMinTempList = new ArrayList<Double>();
                            ArrayList<Double> dayThreeMaxTempList = new ArrayList<Double>();
                            ArrayList<Double> dayThreeMinTempList = new ArrayList<Double>();
                            ArrayList<Double> dayFourMaxTempList = new ArrayList<Double>();
                            ArrayList<Double> dayFourMinTempList = new ArrayList<Double>();
                            ArrayList<Double> dayFiveMaxTempList = new ArrayList<Double>();
                            ArrayList<Double> dayFiveMinTempList = new ArrayList<Double>();

                            Calendar calendarInstance = Calendar.getInstance();

                            for (List weatherList : fiveDayWeatherForecast.getList()) {
                                // get day of the month of the list item
                                Date foreCastItemDate = dateFormat.parse(weatherList.getDtTxt());
                                calendarInstance.setTime(foreCastItemDate);

                                Date foreCastDate = calendarInstance.getTime();

                                int dayDifference =
                                        ((int)((foreCastDate.getTime()/(24*60*60*1000))
                                                -(int)(todayDate.getTime()/(24*60*60*1000))));

                                // add each item to the corresponding date
                                if(dayDifference == 1){
                                    //for each day, save the max and min temperature values in corresponding arraylists
                                    dayOneList.add(weatherList);
                                    dayOneMaxTempList.add(weatherList.getMain().getTempMax());
                                    dayOneMinTempList.add(weatherList.getMain().getTempMin());
                                } else if (dayDifference == 2){
                                    dayTwoList.add(weatherList);
                                    dayTwoMaxTempList.add(weatherList.getMain().getTempMax());
                                    dayTwoMinTempList.add(weatherList.getMain().getTempMin());
                                } else if (dayDifference == 3){
                                    dayThreeList.add(weatherList);
                                    dayThreeMaxTempList.add(weatherList.getMain().getTempMax());
                                    dayThreeMinTempList.add(weatherList.getMain().getTempMin());
                                } else if (dayDifference == 4){
                                    dayFourList.add(weatherList);
                                    dayFourMaxTempList.add(weatherList.getMain().getTempMax());
                                    dayFourMinTempList.add(weatherList.getMain().getTempMin());
                                } else if (dayDifference == 5){
                                    dayFiveList.add(weatherList);
                                    dayFiveMaxTempList.add(weatherList.getMain().getTempMax());
                                    dayFiveMinTempList.add(weatherList.getMain().getTempMin());
                                }
                            }

                            // create arraylist to store forecasts
                            ArrayList<ForeCast> foreCastList = new ArrayList<ForeCast>();

                            //for each day get the weekday, month, max and minimum temperatures
                            Date day1Date = dateFormat.parse(dayOneList.get(0).getDtTxt());
                            calendarInstance.setTime(day1Date);
                            String day1WeekDayName = calendarInstance.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                            int day1DayOfMonth = calendarInstance.get(Calendar.DAY_OF_MONTH);
                            int day1Month = calendarInstance.get(Calendar.MONTH) + 1;
                            Double day1MaxTemp = Collections.max(dayOneMaxTempList);
                            Double day1MinTemp = Collections.min(dayOneMinTempList);

                            ForeCast day1ForeCast = new ForeCast(day1DayOfMonth, day1Month, day1WeekDayName, day1MinTemp, day1MaxTemp);
                            foreCastList.add(day1ForeCast);

                            if (dayTwoList.size() > 0) {
                                Date day2Date = dateFormat.parse(dayTwoList.get(0).getDtTxt());
                                calendarInstance.setTime(day2Date);
                                String day2WeekDayName = calendarInstance.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                                int day2DayOfMonth = calendarInstance.get(Calendar.DAY_OF_MONTH);
                                int day2Month = calendarInstance.get(Calendar.MONTH) + 1;
                                Double day2MaxTemp = Collections.max(dayTwoMaxTempList);
                                Double day2MinTemp = Collections.min(dayTwoMinTempList);

                                ForeCast day2ForeCast = new ForeCast(day2DayOfMonth, day2Month, day2WeekDayName, day2MinTemp, day2MaxTemp);
                                foreCastList.add(day2ForeCast);
                            }

                            Date day3Date = dateFormat.parse(dayThreeList.get(0).getDtTxt());
                            calendarInstance.setTime(day3Date);
                            String day3WeekDayName = calendarInstance.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                            int day3DayOfMonth = calendarInstance.get(Calendar.DAY_OF_MONTH);
                            int day3Month = calendarInstance.get(Calendar.MONTH) + 1;
                            Double day3MaxTemp = Collections.max(dayThreeMaxTempList);
                            Double day3MinTemp = Collections.min(dayThreeMinTempList);

                            ForeCast day3ForeCast = new ForeCast(day3DayOfMonth, day3Month, day3WeekDayName, day3MinTemp, day3MaxTemp);
                            foreCastList.add(day3ForeCast);

                            if (dayFourList.size() > 0) {
                                Date day4Date = dateFormat.parse(dayFourList.get(0).getDtTxt());
                                calendarInstance.setTime(day4Date);
                                String day4WeekDayName = calendarInstance.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                                int day4DayOfMonth = calendarInstance.get(Calendar.DAY_OF_MONTH);
                                int day4Month = calendarInstance.get(Calendar.MONTH) + 1;
                                Double day4MaxTemp = Collections.max(dayFourMaxTempList);
                                Double day4MinTemp = Collections.min(dayFourMinTempList);

                                ForeCast day4ForeCast = new ForeCast(day4DayOfMonth, day4Month, day4WeekDayName, day4MinTemp, day4MaxTemp);
                                foreCastList.add(day4ForeCast);
                            }

                            if (dayFiveList.size() > 0) {
                                Date day5Date = dateFormat.parse(dayFiveList.get(0).getDtTxt());
                                calendarInstance.setTime(day5Date);
                                String day5WeekDayName = calendarInstance.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                                int day5DayOfMonth = calendarInstance.get(Calendar.DAY_OF_MONTH);
                                int day5Month = calendarInstance.get(Calendar.MONTH) + 1;
                                Double day5MaxTemp = Collections.max(dayFiveMaxTempList);
                                Double day5MinTemp = Collections.min(dayFiveMinTempList);

                                ForeCast day5ForeCast = new ForeCast(day5DayOfMonth, day5Month, day5WeekDayName, day5MinTemp, day5MaxTemp);
                                foreCastList.add(day5ForeCast);
                            }

                            //update the adapter to show the 5 day forecast
                            if (foreCastList.size() > 0) {
                                mAdapter.setForeCastList(foreCastList);
                            }

                            mProgressBar.setVisibility(View.INVISIBLE);

                        } catch (ParseException p) {
                            p.printStackTrace();
                            Log.e(TAG, p.getMessage());
                            mProgressBar.setVisibility(View.INVISIBLE);
                            showErrorView();
                        } catch (Exception e){
                            e.printStackTrace();
                            Log.e(TAG, e.getMessage());
                            mProgressBar.setVisibility(View.INVISIBLE);
                            showErrorView();
                        }

                    } else {
                        Log.d(TAG, "Code: " + response.code() + " Message: " + response.message());
                        mProgressBar.setVisibility(View.INVISIBLE);
                        showErrorView();
                    }
                }

                @Override
                public void onFailure(Call<WeatherForecast> call, Throwable t) {
                    Log.d(TAG, t.getMessage());
                    mProgressBar.setVisibility(View.INVISIBLE);
                    showErrorView();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            showErrorView();
        }

    }
}
