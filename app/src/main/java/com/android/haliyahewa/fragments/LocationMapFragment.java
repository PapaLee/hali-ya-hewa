package com.android.haliyahewa.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.haliyahewa.R;
import com.android.haliyahewa.models.Coord;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by papalee on 9/19/17.
 */

public class LocationMapFragment extends SupportMapFragment implements GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, OnMapReadyCallback {
    private GoogleMap mMap;
    private Button mDoneButton;
    private ArrayList<LatLng> latLngArrayList = new ArrayList<LatLng>();
    private ArrayList<Coord> mCoordList;
    private LocationMapFragment.IMapListener mListener;

    @Override
    public void onMapClick(LatLng latLng) {
        // add a marker/location/pin when the user taps on the map
        mMap.addMarker(new MarkerOptions().position(latLng));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10));

        // as well as add it to the arraylist of LatLngs
        latLngArrayList.add(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //remove the location when the user taps on the marker
        latLngArrayList.remove(marker.getPosition());
        marker.remove();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // enable the map and marker listeners
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);

        //enable zoom controls
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    public interface IMapListener
    {
        void loadMapLocationCoordinates(ArrayList<Coord> coordArrayList);
    }

    public LocationMapFragment(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof LocationMapFragment.IMapListener) {
            mListener = (LocationMapFragment.IMapListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IMapListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_map, container, false);

        if(mMap == null) {
            SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            mapFragment.getMapAsync(this);
        }

        mDoneButton = (Button) v.findViewById(R.id.btn_done);
        mDoneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // clear the previous list of cooordinates, if any
                if(mCoordList != null){
                    mCoordList.clear();
                } else{
                    mCoordList = new ArrayList<Coord>();
                }

                // populate the Arraylist of coordinates from the Arraylist of LatLngs
                for (LatLng latLng : latLngArrayList) {
                    Coord coord = new Coord(latLng.latitude, latLng.longitude);
                    mCoordList.add(coord);
                }

                // load the location list fragment and pass the list of location coordinates
                mListener.loadMapLocationCoordinates(mCoordList);
            }
        });

        return v;
    }
}
