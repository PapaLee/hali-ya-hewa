package com.android.haliyahewa;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.haliyahewa.adapters.WeatherContainerAdapter;
import com.android.haliyahewa.apis.OpenWeatherMapApi;
import com.android.haliyahewa.fragments.DetailFragment;
import com.android.haliyahewa.fragments.HelpFragment;
import com.android.haliyahewa.fragments.LocationMapFragment;
import com.android.haliyahewa.models.Coord;
import com.android.haliyahewa.models.WeatherContainer;
import com.android.haliyahewa.utilities.WeatherPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.android.haliyahewa.Constants.DEFAULT_UNITS;
import static com.android.haliyahewa.Constants.OWM_API_KEY;
import static com.android.haliyahewa.Constants.OWM_BASE_URL;


public class MainActivity extends AppCompatActivity implements WeatherContainerAdapter.WeatherContainerAdapterOnClickHandler, LocationMapFragment.IMapListener{
    private RecyclerView mRecyclerView;
    private ArrayList<Coord> mCoordList;
    private WeatherContainerAdapter mAdapter;
    private TextView mTvError;
    private ProgressBar mProgressBar;
    private WeatherPreferences mSharedPrefs;
    private Button mButtonAddLocation;
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mButtonAddLocation = (Button)findViewById(R.id.btn_add_location);
        mButtonAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //load the map fragment
                LocationMapFragment mapFragment = new LocationMapFragment();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.base_fragment_container, mapFragment, Constants.MAPFRAGMENT_TAG)
                        // Add this transaction to the back stack
                        .addToBackStack(null)
                        .commit();
            }
        });

        mButtonAddLocation = (Button)findViewById(R.id.btn_add_location);
        mRecyclerView = (RecyclerView) findViewById(R.id.location_list);
        mTvError = (TextView) findViewById(R.id.tv_error);
        mProgressBar = (ProgressBar) findViewById(R.id.pb_busy);

        if (findViewById(R.id.location_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mAdapter = new WeatherContainerAdapter(new ArrayList<WeatherContainer>(), MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        //if there are any bookmarked locations, retrieve them from shared preferences
        mSharedPrefs = new WeatherPreferences(this);
        mCoordList = new ArrayList<Coord>();

        ArrayList<Coord> coordBookMarkArrayList = null;

        if (mSharedPrefs.isKeyPresent(Constants.BOOKMARKLIST_KEY)){
            coordBookMarkArrayList = mSharedPrefs.getLocationBookMarkList(Constants.BOOKMARKLIST_KEY);
            mCoordList.addAll(coordBookMarkArrayList);
        }

        //mCoordList.add(new Coord(Double.valueOf(Constants.DEFAULT_LAT), Double.valueOf(Constants.DEFAULT_LON)));

        if((mCoordList != null && !mCoordList.isEmpty())){
            fetchWeatherData(mCoordList);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            //load settings fragment
            return true;
        }

        if (id == R.id.action_help) {
            // load help fragment
            loadHelpFragment();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadFragment(Fragment fragment, int holder, String tag){
        // adds a fragment into a container view

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        transaction.add(holder, fragment, tag);
        transaction.commit();
    }

    private void replaceFragment(Fragment fragment, int holder, String tag){
        // replaces a fragment in a container view

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        transaction.replace(holder, fragment, tag);
        transaction.commit();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void loadHelpFragment(){
        FragmentManager fm = getSupportFragmentManager();

        Fragment frag = fm.findFragmentByTag(Constants.HELPFRAGMENT_TAG);
        if (frag != null) {
            fm.beginTransaction().remove(frag).commit();
        }
        fm.beginTransaction().addToBackStack(null);

        HelpFragment helpDialogFragment = new HelpFragment();
        helpDialogFragment.show(fm, Constants.HELPFRAGMENT_TAG);
    }

    private void fetchWeatherData(ArrayList<Coord> coordArrayList) {
        showWeatherView();
        new FetchWeatherDataTask().execute(coordArrayList);
    }

    private void showWeatherView() {
        //hide the list, show the error
        mTvError.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorView(){
        //hide the error view, show the list
        mRecyclerView.setVisibility(View.INVISIBLE);
        mTvError.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLocationItemClick(WeatherContainer weatherContainer) {
        Log.d(TAG, "Location Item Clicked");

        if (mTwoPane) {
            // load the location detail fragment and pass the weather container object clicked from the recyclerview
            if( weatherContainer != null){
                DetailFragment locationDetailFragment = new DetailFragment();

                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.WEATHER_ARGS, weatherContainer);
                locationDetailFragment.setArguments(bundle);

                replaceFragment(locationDetailFragment, R.id.location_detail_container, Constants.DETAILFRAGMENT_TAG);
            }

        } else {
            Log.d(TAG, "Single Pane Location Item Clicked");
            DetailFragment locationDetailFragment = new DetailFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.WEATHER_ARGS, weatherContainer);
            locationDetailFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.base_fragment_container, locationDetailFragment, Constants.DETAILFRAGMENT_TAG)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onLocationItemBookMarked(Coord coord) {
        // saves the bookmarked location to the shared preferences
        mSharedPrefs.addLocationBookMark(coord);

        Toast.makeText(this, R.string.bookmark_added,
                Toast.LENGTH_LONG).show();

        Log.d(TAG, "Location ItemBookMarked");
    }

    @Override
    public void onLocationDeleted(Coord coord) {
        // remove the location from the shared preferences, if it was bookmarked
        mSharedPrefs.deleteLocationFromBookMark(coord);

        //remove the map fragment
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentByTag(Constants.DETAILFRAGMENT_TAG);
        if (frag != null) {
            fm.beginTransaction().remove(frag).commit();
        }

        Log.d(TAG, "Location Item deleted");
    }

    @Override
    public void loadMapLocationCoordinates(ArrayList<Coord> coordArrayList) {

        //remove the map fragment
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentByTag(Constants.MAPFRAGMENT_TAG);
        if (frag != null) {
            fm.beginTransaction().remove(frag).commit();
        }

        //on receiving the arraylist of map coordinates from the map fragment, call the openWeatherMap api to fetch weather details
        if (coordArrayList != null && !coordArrayList.isEmpty()) {

            //if there exists bookmarked coordinates, the add them to the list before calling the api
            if((mCoordList != null && !mCoordList.isEmpty())){
                mCoordList.addAll(coordArrayList);
                fetchWeatherData(mCoordList);
            } else{
                fetchWeatherData(coordArrayList);
            }
        }
    }

    public class FetchWeatherDataTask extends AsyncTask<ArrayList<Coord>, Void, ArrayList<WeatherContainer>> {
        // fetches the forecast for each coordinate afterwhich it updates the adapter
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<WeatherContainer> doInBackground(ArrayList<Coord>... params) {
            ArrayList<WeatherContainer> weatherContainerArrayList = new ArrayList<WeatherContainer>();

            if (params.length == 0) {
                return null;
            }

            try {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(OWM_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                OpenWeatherMapApi apiService = retrofit.create(OpenWeatherMapApi.class);

                if(params[0] != null && !params[0].isEmpty()){
                    for (Coord coord : params[0]) {

                        // call weather api against each coordinate
                        Call<WeatherContainer> weatherContainerCall = apiService.getDayForecast(coord.getLat().toString(), coord.getLon().toString(), OWM_API_KEY, DEFAULT_UNITS);
                        Response<WeatherContainer> response = weatherContainerCall.execute();

                        if(response.isSuccessful()) {
                            weatherContainerArrayList.add(response.body());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return weatherContainerArrayList;
        }

        @Override
        protected void onPostExecute(ArrayList<WeatherContainer> weatherContainerArrayList) {
            mProgressBar.setVisibility(View.INVISIBLE);

            if (weatherContainerArrayList != null) {
                showWeatherView();

                //update the adapter with results from the api
                mAdapter.setWeatherContainerList(weatherContainerArrayList);

            } else {
                showErrorView();
            }
        }
    }
}
