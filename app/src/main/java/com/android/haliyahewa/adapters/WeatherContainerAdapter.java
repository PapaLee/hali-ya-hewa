package com.android.haliyahewa.adapters;

/**
 * Created by papalee on 9/13/17.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.haliyahewa.R;
import com.android.haliyahewa.models.Coord;
import com.android.haliyahewa.models.WeatherContainer;
import com.android.haliyahewa.utilities.Helper;

import java.util.ArrayList;

public class WeatherContainerAdapter extends RecyclerView.Adapter<WeatherContainerAdapter.WeatherContainerViewHolder>{

    private ArrayList<WeatherContainer> mWeatherContainerList;
    private static final String TAG = WeatherContainerAdapter.class.getSimpleName();
    private final WeatherContainerAdapterOnClickHandler mClickHandler;

    public interface WeatherContainerAdapterOnClickHandler {
        void onLocationItemClick(WeatherContainer weatherContainer);
        void onLocationItemBookMarked(Coord coord);
        void onLocationDeleted(Coord coord);
    }

    public WeatherContainerAdapter(ArrayList<WeatherContainer> items, WeatherContainerAdapterOnClickHandler mClickHandler){
        this.mWeatherContainerList = items;
        this.mClickHandler = mClickHandler;
    }

    @Override
    public WeatherContainerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.locationlist_item, parent, false);
        return new WeatherContainerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WeatherContainerViewHolder holder, final int position) {
        holder.weatherContainer = mWeatherContainerList.get(position);

        if(holder.weatherContainer != null) {

            try {
                String locationName;

                if(!holder.weatherContainer.getName().isEmpty() && !holder.weatherContainer.getSys().getCountry().isEmpty())
                {
                    locationName = holder.weatherContainer.getName() + ", " + holder.weatherContainer.getSys().getCountry();
                } else {
                    locationName = holder.weatherContainer.getSys().getCountry();
                }

                holder.tvLocationName.setText(locationName);

                String weatherDescription = holder.weatherContainer.getMain().getTemp().toString() + "°, " + Helper.capitalizeFirstLetter(holder.weatherContainer.getWeather().get(0).getDescription());
                holder.tvLocationForecast.setText(weatherDescription);

               holder.imgBtnDelBookMark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // deletes the location from the list

                        Coord coord = mWeatherContainerList.get(holder.getAdapterPosition()).getCoord();

                        mWeatherContainerList.remove(holder.getAdapterPosition());
                        notifyItemRemoved(holder.getAdapterPosition());

                        // delete the location bookmark from sharedpreferences
                        mClickHandler.onLocationDeleted(coord);
                    }
                });

                holder.imgBtnAddBookMark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // adds the Coordinates of the clicked location to be persisted in the bookmark list
                        Coord bookMarkCoord = mWeatherContainerList.get(holder.getAdapterPosition()).getCoord();

                        if(bookMarkCoord != null) {
                            mClickHandler.onLocationItemBookMarked(bookMarkCoord);
                        }

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, e.getMessage());

            }
        }
    }

    @Override
    public int getItemCount() {
        return mWeatherContainerList.size();
    }

    public void setWeatherContainerList(ArrayList<WeatherContainer> items) {
        mWeatherContainerList = items;
        notifyDataSetChanged();
    }

    public class WeatherContainerViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public TextView tvLocationName;
        public TextView tvLocationForecast;
        public ImageButton imgBtnAddBookMark;
        public ImageButton imgBtnDelBookMark;
        //public TextView locationDelete;
        public WeatherContainer weatherContainer;

        public WeatherContainerViewHolder(View itemView) {
            super(itemView);

            tvLocationName = (TextView) itemView.findViewById(R.id.tv_location_name);
            tvLocationForecast = (TextView) itemView.findViewById(R.id.tv_location_forecast);
            //locationDelete = (TextView) itemView.findViewById(R.id.tv_delete_location);
            imgBtnAddBookMark = (ImageButton) itemView.findViewById(R.id.btn_add_bookmark);
            imgBtnDelBookMark = (ImageButton) itemView.findViewById(R.id.btn_del_bookmark);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            // open the location detail fragment when the recycleview item is clicked/tapped
            WeatherContainer clickedWeatherContainer = mWeatherContainerList.get(getAdapterPosition());
            mClickHandler.onLocationItemClick(clickedWeatherContainer);
        }
    }
}
