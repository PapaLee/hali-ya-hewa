package com.android.haliyahewa.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.haliyahewa.R;
import com.android.haliyahewa.models.ForeCast;

import java.util.ArrayList;

/**
 * Created by papalee on 9/20/17.
 */

public class ForeCastAdapter extends RecyclerView.Adapter<ForeCastAdapter.ForeCastViewHolder>{

    private ArrayList<ForeCast> mForeCastList;

    public ForeCastAdapter(ArrayList<ForeCast> items){
        this.mForeCastList = items;
    }

    @Override
    public ForeCastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forecastlist_item, parent, false);
        return new ForeCastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForeCastViewHolder holder, int position) {
        holder.foreCast = mForeCastList.get(position);

        //Math.round(Math.floor(Double.parseDouble(locationMapObject.getMain().getTemp())));
        if(holder.foreCast != null){
            String weekName = holder.foreCast.getWeekDayName();
            String date = holder.foreCast.getDayOfMonth() + "/" + holder.foreCast.getMonth();

            holder.tvDayOfWeekName.setText(weekName + " - " + date);
            //holder.tvDate.setText();

            long maxTemp = Math.round(Math.floor(holder.foreCast.getMaxTemp()));
            long minTemp = Math.round(Math.floor(holder.foreCast.getMinTemp()));
            holder.tvMinMaxTemp.setText(maxTemp +  "°/" + minTemp+ "°");
        }

    }

    @Override
    public int getItemCount() {
        return mForeCastList.size();
    }

    public class ForeCastViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDayOfWeekName;
        public TextView tvDate;
        public TextView tvMinMaxTemp;
        public ForeCast foreCast;

        public ForeCastViewHolder(View itemView) {
            super(itemView);

            tvDayOfWeekName = (TextView) itemView.findViewById(R.id.tv_day_of_week);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvMinMaxTemp = (TextView) itemView.findViewById(R.id.tv_min_max_temp);
        }

    }

    public void setForeCastList(ArrayList<ForeCast> items) {
        mForeCastList = items;
        notifyDataSetChanged();
    }
}
