package com.android.haliyahewa;

import android.util.Log;

import com.android.haliyahewa.apis.OpenWeatherMapApi;
import com.android.haliyahewa.models.WeatherContainer;

import org.junit.Test;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static org.junit.Assert.*;

import static com.android.haliyahewa.Constants.DEFAULT_LAT;
import static com.android.haliyahewa.Constants.DEFAULT_LON;
import static com.android.haliyahewa.Constants.DEFAULT_UNITS;
import static com.android.haliyahewa.Constants.OWM_API_KEY;
import static com.android.haliyahewa.Constants.OWM_BASE_URL;

/**
 * Created by papalee on 9/12/17.
 */

public class OpenWeatherMapApiTest {
    private static final String TAG = OpenWeatherMapApiTest.class.getSimpleName();

    @Test
    public void IsDayForecastValid() throws Exception {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OWM_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeatherMapApi apiService = retrofit.create(OpenWeatherMapApi.class);
        Call<WeatherContainer> weatherContainerCall = apiService.getDayForecast(DEFAULT_LAT, DEFAULT_LON, OWM_API_KEY, DEFAULT_UNITS);

        weatherContainerCall.enqueue(new retrofit2.Callback<WeatherContainer>() {
            @Override
            public void onResponse(Call<WeatherContainer> call, retrofit2.Response<WeatherContainer> response) {
                if(response.isSuccessful()) {
                    WeatherContainer weatherContainer = response.body();

                    assertEquals(weatherContainer.getId().toString(), "184745");
                } else {
                    Log.d(TAG, "Code: " + response.code() + " Message: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<WeatherContainer> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }
}
